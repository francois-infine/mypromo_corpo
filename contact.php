<?php
	// mail("francois@infine-digital.be","Contact",$_POST['email']);
	file_put_contents('contact.txt'
	       ,date('r') . ':' . var_export($_POST, true)
	       , FILE_APPEND);

	// =====
	$mail = 'gerald.schelkens@infine.net, lucie.buttiens@infine.net'; // Déclaration de l'adresse de destination.
	if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
	{
		$passage_ligne = "\r\n";
	}
	else
	{
		$passage_ligne = "\n";
	}
	//=====Déclaration des messages au format texte et au format HTML.
	$message_txt = $_POST['email']." asked to be contacted (via mypromo.be). You can reply to this e-mail to get in touch directly.";
	$message_html = "<html><head></head><body>".$_POST['email']." asked to be contacted (via mypromo.be). You can reply to this e-mail to get in touch directly.</body></html>";
	//==========
	 
	//=====Création de la boundary
	$boundary = "-----=".md5(rand());
	//==========
	 
	//=====Définition du sujet.
	$sujet = "Contact from mypromo.be";
	//=========
	 
	//=====Création du header de l'e-mail.
	$header = "From: \"contact\"<contact@mypromo.be>".$passage_ligne;
	$header.= "Reply-to: ".$_POST['email'].$passage_ligne;
	$header.= "MIME-Version: 1.0".$passage_ligne;
	$header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
	//==========
	 
	//=====Création du message.
	$message = $passage_ligne."--".$boundary.$passage_ligne;
	//=====Ajout du message au format texte.
	$message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
	$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
	$message.= $passage_ligne.$message_txt.$passage_ligne;
	//==========
	$message.= $passage_ligne."--".$boundary.$passage_ligne;
	//=====Ajout du message au format HTML
	$message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
	$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
	$message.= $passage_ligne.$message_html.$passage_ligne;
	//==========
	$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
	$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
	//==========
	 
	//=====Envoi de l'e-mail.
	mail($mail,$sujet,$message,$header);
	//==========
?>
<?php include('head.php') ?>
	<main>
        <?php include('header.php') ?>
        <section class="container">
        	Thank you <?php echo $_POST['email']; ?>, we will get in touch with you soon.
        </section>
    </main>
<?php include('footer.php') ?>