    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <h2>Let’s work together</h2>
                    If you'd like to have more info about the MyPromo alternative, please give us your
                    <form action="contact.php" method="post" class="form-inline">
                        <input type="email" name="email" required placeholder="e-mail address" class="">
                        <input type="submit" value="OK" class="">
                    </form>
                    <br>
                    and we will contact you back.
                    <br>
                    <br>
                    <span>More about us:</span> <a href="http://www.infine.net" target="_blank">www.infine.net</a>
                </div>
                <div class="col-sm-4 col-sm-offset-3">
                    <h2>Who we are</h2>
                    <p><a href="http://www.infine.net" target="_blank" rel="author"><img src="assets/img/logo-in_fine.php?fill=000" alt="In Fine" width="48"></a> is an advertising agency in Liège, with expertise in creating multimedia, graphic design, web and mobile design.</p>
                </div>
            </div>
        </div>
    </footer>
    
    <script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>
    <script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js?1.11.1"><\/script>')</script>
    <script type='text/javascript' src='assets/js/main.js'></script>
  </body>
</html>