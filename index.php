<?php include('head.php') ?>
    <main>
        <?php include('header.php') ?>
        <section class="container">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe width="1140" height="641" src="https://www.youtube.com/embed/kd7JxJgIwu8" frameborder="0" allowfullscreen class="embed-responsive-item"></iframe>
            </div>
        </section>
    </main>
<?php include('footer.php') ?>