<?php header('X-UA-Compatible: IE=edge'); ?>
<!doctype html>
<?php if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE 10')): ?>
<html class="ie ie10">
<?php elseif (stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')): ?>
<html class="ie ie11">
<?php else: ?>
<!--[if !IE]><!--><html class="no-js" lang="fr"><!--<![endif]-->
<?php endif; ?>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- entre 15 et 55 char -->
    <title>MyPromo - The 100% online payback solution</title>
    <!-- entre 50 et 160 char -->
    <meta name="description" content="MyPromo, The 100% online payback solution - by In Fine" />

    <link rel="alternate" type="application/rss+xml" title="" href="">

    <!-- taille origine: 260x260 -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <meta property="og:url" content="http://www.mypromo.be" />
    <meta property="og:type" content="website" />
    <!-- The title of your article. This shouldn't include branding such as your site name. -->
    <meta property="og:title" content="MyPromo" />
    <!-- A brief description of the content, usually between 2 and 4 sentences. This will displayed below the title of the post on Facebook. -->
    <meta property="og:description" content="MyPromo, The 100% online payback solution" />
    <!-- The name of your website (such as IMDb, not imdb.com). -->
    <meta property="og:site_name" content="MyPromo" />
    <!-- best for hi-res: 1200x630px - minimum: 600x315px - ideal ratio: 1.91:1 -->
    <meta property="og:image" content="/ogimage.jpg" />
    <meta name="pinterest" content="nohover">

    <link rel="stylesheet" href="assets/css/main.css" type="text/css" media="all" />
    
    <link type="text/plain" rel="author" href="/humans.txt" />

    <!-- ie-css -->
    <!--[if IE]>
    <link rel='stylesheet' id='ie-all.css-css' href='/assets/css/ie-all.css' type='text/css' media='all' />
    <![endif]-->
    <!--[if lte IE 9]>
    <link rel='stylesheet' id='ie9.css-css' href='/assets/css/ie9.css' type='text/css' media='all' />
    <![endif]-->

    <!-- ie-js -->
    <!--[if (lte IE 8)]>
      <script type="text/javascript" src="assets/vendor/html5shiv/dist/html5shiv.min.js"></script>
      <script type="text/javascript" src="assets/vendor/respond/dest/respond.min.js"></script>
    <![endif]-->
    <!--[if (lte IE 9)]>
      <script type="text/javascript" src="assets/vendor/placeholders/dist/placeholders.min.js"></script>
    <![endif]-->
    <script src="https://use.typekit.net/obb8xgy.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
  </head>
  <body>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-85998577-1', 'auto');
      ga('send', 'pageview');
    </script>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
      </div>
    <![endif]-->