var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var notify = require("gulp-notify");

// -----------------------------------
// variables
// -----------------------------------
var paths = {
	js: ['assets/js/'],
	js_watch: [
		'assets/js/main.js',
		'assets/js/plugins/*.js',
		'assets/js/plugins/**/*.js'
	],
	js_vendor: ['assets/js/vendor/*.js'],
	less: 'assets/less/',
	sass: 'assets/sass/',
	css: 'assets/css/',
	html: ['*.php', 'templates/**/*.php'],
	images: ['assets/img/'],
	vectors: ['assets/img/**/*.svg'],
	// extras: ['humans.txt', 'robots.txt'],
	styleguide: ['styleguide/'],
	favicons: [
		'assets/favicon/apple-touch-icon-57x57.png',
		'assets/favicon/apple-touch-icon-114x114.png',
		'assets/favicon/apple-touch-icon-72x72.png',
		'assets/favicon/apple-touch-icon-144x144.png',
		'assets/favicon/apple-touch-icon-60x60.png',
		'assets/favicon/apple-touch-icon-120x120.png',
		'assets/favicon/apple-touch-icon-76x76.png',
		'assets/favicon/apple-touch-icon-152x152.png',
		'assets/favicon/apple-touch-icon-180x180.png',
		'assets/favicon/favicon-160x160.png',
		'assets/favicon/favicon-96x96.png',
		'assets/favicon/favicon-16x16.png',
		'assets/favicon/favicon-32x32.png'
	]
};

gulp.task('default',['watch-svg', 'serve']);

// -----------------------------------
// serve
// -----------------------------------
// gulp serve --proxy moncheminlocal
gulp.task('serve', function() {
    
    var argv = require('yargs').argv;
    if (argv.p) {
	    browserSync.init({
	        proxy: argv.p,
	        // notify: true
	    });
    } else if (argv.proxy) {
    	browserSync.init({
	        proxy: argv.proxy,
	        // notify: true
	    });
    } else {
		var baseurl = __dirname.match(/Sites(.*)/ig)[0];
		baseurl = baseurl.replace('Sites', 'localhost:8888');
		baseurl = baseurl.replace('wp-content', '');
	    browserSync.init({
	        proxy: baseurl,
	        // notify: true
	    });
    }

    gulp.watch(paths.js_watch).on('change', browserSync.reload);
    gulp.watch(paths.images+"**/*.{jpg,jpeg,JPG,JPEG,png,PNG,gif,GIF}", ['gulp-images']);
    // gulp.watch(paths.images+"**/*.{svg,SVG}", ['svg']);

    gulp.watch([
    	"**/*.php",
    ]).on('change', browserSync.reload);

    gulp.watch(paths.less+"**/*.less", ['less']);
    // gulp.watch(paths.less+"**/*.less", ['hologram', 'less-styleguide']);
    
    // styleguide
    // gulp.watch([paths.styleguide+'**/*.html'], ['hologram']);
    // gulp.watch(paths.styleguide+'less/*.less', ['less-styleguide']);
});

// -----------------------------------
// less
// -----------------------------------
// Compile sass into CSS & auto-inject into browsers
gulp.task('less', function() {
	var less = require('gulp-less');
	var lessPluginGlob = require('less-plugin-glob');
	var postcss = require('gulp-postcss');
	var minifyCSS = require('gulp-minify-css');
	var filter = require('gulp-filter');
	var autoprefixer = require('autoprefixer');
	var sourcemaps = require('gulp-sourcemaps');
	var processors = [
		autoprefixer
	];

    return gulp.src(paths.less+"main.less")
        .pipe(sourcemaps.init())
        .pipe(less({
			plugins: [lessPluginGlob]
		}))
		.on("error", notify.onError(function (error) {return error.message;}))
        .pipe(minifyCSS())
        // .pipe(notify('less ftw!'))
        .pipe(postcss(processors))
    	.on("error", notify.onError(function (error) {return error.message;}))
        .pipe(notify('postcss'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.css))
        .pipe(filter(['**/*.css']))
        .pipe(browserSync.stream());
        // .pipe(browserSync.reload({stream: true}));
});

gulp.task('compass', function(cb) {
	var compass = require('gulp-compass');
	var postcss = require('gulp-postcss');
	var autoprefixer = require('autoprefixer');
	var processors = [
		autoprefixer
	];
	return gulp.src(paths.sass+'main.scss')
		.pipe(compass({
			config_file: paths.sass+'config.rb',
			css: paths.css,
			sass: paths.sass
		}))
		.on("error", notify.onError(function (error) {return error.message;}))
		.pipe(postcss(processors))
		.pipe(gulp.dest(paths.css))
		.pipe(browserSync.stream());
});

// -----------------------------------
// images
// -----------------------------------
gulp.task('gulp-images', function () {
	var image = require('gulp-image'),
	imagemin = require('gulp-imagemin'),
	imageOptim = require('gulp-imageoptim'),
	changed = require('gulp-changed');

	return gulp.src(paths.images+'original/**/*.{jpg,jpeg,JPG,JPEG,png,PNG,gif,GIF}')
	    .pipe(changed(paths.images+'min'))
	    .pipe(image())
	    .pipe(gulp.dest(paths.images+'min'));
});

gulp.task('watch-svg', function() {
    gulp.watch(paths.images+"original/*.{svg,SVG}").on('change', function(event) {
    	console.log(event.path);
    	var svgmin = require('gulp-svgmin');
		var rename = require('gulp-rename');

		return gulp.src(event.path)
		    .pipe(svgmin({
		    	js2svg: {
	                pretty: true
	            },
	            plugins: [
		            {
		                removeDoctype: false
		            }, {
		                removeComments: true
		            }, {
		                cleanupNumericValues: {
		                    floatPrecision: 2
		                }
		            }, {
		                convertColors: {
		                    names2hex: false,
		                    rgb2hex: false
		                }
		            }, {
		            	removeDimensions: false
		            },
		            {
		            	addDimensions: true
		            }
	            ]
	        }))
	        .pipe(rename({
			    extname: ".min.svg"
			}))
		    .pipe(gulp.dest(paths.images+'min'))
    });
})

gulp.task('imagemin', function () {
	var pngquant = require('imagemin-pngquant');
	var imagemin = require('gulp-imagemin');
    // return gulp.src(paths.images+'test/**/*.{jpg,jpeg,JPG,JPEG,png,PNG,gif,GIF,svg,SVG}')
    return gulp.src(paths.images+'original/**/*.{jpg,jpeg,JPG,JPEG,png,PNG,gif,GIF,svg,SVG}')
        .pipe(imagemin({
            // progressive: true,
            // interlaced: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(paths.images+'imagemin'));
});

gulp.task('imageoptim', function() {
	var imageOptim = require('gulp-imageoptim');
    return gulp.src(paths.images+'test/**/*')
        .pipe(imageOptim.optimize())
        .pipe(gulp.dest(paths.images+'imageoptim'));
});

// -----------------------------------
// styleguide
// -----------------------------------
gulp.task('hologram', function() {
	var hologram = require('gulp-hologram');
	gulp.src(paths.styleguide+'hologram_config.yml')
		.pipe(hologram())
		.pipe(notify('styleguide generated'));
});

gulp.task('less-styleguide', function() {
	var less = require('gulp-less');
	var lessPluginGlob = require('less-plugin-glob');
	var postcss = require('gulp-postcss');
	var minifyCSS = require('gulp-minify-css');
	var filter = require('gulp-filter');
	var autoprefixer = require('autoprefixer');
	var sourcemaps = require('gulp-sourcemaps');
	var processors = [
		autoprefixer
	];

	return gulp.src(paths.less+"styleguide.less")
        .pipe(sourcemaps.init())
        .pipe(less({
			plugins: [lessPluginGlob]
		}))
		.on("error", notify.onError(function (error) {return error.message;}))
        .pipe(minifyCSS())
        // .pipe(notify('less ftw!'))
        .pipe(postcss(processors))
    	.on("error", notify.onError(function (error) {return error.message;}))
        .pipe(notify('postcss'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.css))
        .pipe(filter(['**/*.css']))
        .pipe(browserSync.stream());
})