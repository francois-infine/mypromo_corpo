$(document).ready(function() {
  if (window.location.hash) {
    setTimeout(function() {
      window.scrollTo(0, 0);
    }, 1);
    $('[href="'+window.location.hash+'"]').trigger('click');
  }

  $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    location.hash = $(e.target).attr('href');
  })
});