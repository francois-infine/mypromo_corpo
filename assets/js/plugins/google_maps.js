var themap = themap || {
	
};

themap.render = function ($el) {
	// vars
	var $markers = $($el).find('.marker');
	var styleName = $($el).attr('data-style');
	
	var zoom = Number($el.attr('data-zoom'));
	var isDraggable = $('html').hasClass('touchevents') ? false : true;

	var num = styleName;
  	var styleJson = window['style_' + num];

	// args
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false,
		draggable: isDraggable,
	};

	// create map        	
	// var map = new google.maps.Map( $el[0], args);
	var map = themap.create($($el), args);

	// add a markers reference
	map.markers = [];

	// add markers
	$markers.each(function(){
    	themap.addMarker( $(this), map);
	});

	// center map
	themap.center(map, zoom);

	// style map
	themap.style(map, styleJson);

	return({
        container:map.getDiv(),
        mapInstance:map
    });
}

themap.create = function($el, args) {
	var map = new google.maps.Map($el[0], args);
	return map;
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

themap.addMarker = function ( $marker, map ) {

	// vars
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	var markerIcon = $marker.attr('data-icon');

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map,
		icon		: markerIcon,
		optimized	: false
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}
}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

themap.center = function(map, zoom) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 ) {
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom(zoom);
	} else {
		// fit to bounds
		map.fitBounds( bounds );
	}
}

/*
*  style_map
*
*  This function will style the map. The page should have a json named "style_myname" and the map instance should have a data-style="myname"
*
*  @type	function
*  @date	26/04/2016
*  @since	4.3.0
*
*  @param	$map (jQuery element)
*  @param	style (json object)
*  @return	n/a
*/
themap.style = function(map, style) {
	map.set('styles', style);
}

themap.reinit = function(map) {
	google.maps.event.trigger(map, 'resize');
};